package pl.schibsted.recruitment.number.pronunciation.english;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class EnglishNumberPronouncerTest {
    private final Integer givenNumber;
    private final String expectedPronunciation;
    private static EnglishNumberPronouncer englishNumberPronouncer;

    @Parameterized.Parameters
    public static Collection<Object[]> givenData() {
        return Arrays.asList(
                new Object[][]{
                        {1, "one"}, {2, "two"}, {3, "three"}, {4, "four"}, {5, "five"},
                        {6, "six"}, {7, "seven"}, {8, "eight"}, {9, "nine"}, {10, "ten"},
                        {11, "eleven"}, {12, "twelve"}, {13, "thirteen"}, {20, "twenty"}, {21, "twenty-one"},
                        {30, "thirty"}, {100, "one hundred"}, {400, "four hundred"}, {405, "four hundred five"},
                        {421, "four hundred twenty-one"}, {659, "six hundred fifty-nine"}, {782, "seven hundred eighty-two"},
                        {1500, "one thousand five hundred",}, {760652, "seven hundred sixty thousand six hundred fifty-two"},
                        {1000000, "one million"}, {1111111, "one million one hundred eleven thousand one hundred eleven"},
                        {Integer.MAX_VALUE, "two billion one hundred forty-seven million " +
                                "four hundred eighty-three thousand six hundred forty-seven"},
                        {0, "zero"}, {-1, "minus one"}, {-111, "minus one hundred eleven"}
                });
    }

    @BeforeClass
    public static void setUp() {
        englishNumberPronouncer = new EnglishNumberPronouncer();
    }

    public EnglishNumberPronouncerTest(Integer givenNumber, String expectedPronunciation) {
        this.givenNumber = givenNumber;
        this.expectedPronunciation = expectedPronunciation;
    }

    @Test
    public void shouldConvertNumberToPronunciation() {
        // when
        String pronouncedNumber = englishNumberPronouncer.pronounce(givenNumber);

        // then
        Assert.assertEquals("Pronounced number for " + givenNumber + " is incorrect!", expectedPronunciation, pronouncedNumber);
    }
}
