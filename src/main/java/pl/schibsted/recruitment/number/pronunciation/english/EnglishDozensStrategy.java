package pl.schibsted.recruitment.number.pronunciation.english;

import com.google.common.collect.ImmutableMap;
import pl.schibsted.recruitment.number.pronunciation.DozensStrategy;

import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

enum EnglishDozensStrategy implements DozensStrategy {
    UNDER_TEN(i -> i >= 0 && i < 10) {
        @Override
        public String processDozens(int dozens) {
            return EnglishUnitsStrategy.getEligibleStrategy(dozens).processUnits(dozens);
        }
    }, BETWEEN_TEN_AND_TWENTY(i -> i >= 10 && i < 20) {
        @Override
        public String processDozens(int dozens) {
            return CONSTANTS_MAPPING.get(dozens);
        }
    }, TWENTY_AND_OVER(i -> i >= 20) {
        @Override
        public String processDozens(int dozens) {
            int units = dozens % 10;
            EnglishUnitsStrategy unitsStrategy = EnglishUnitsStrategy.getEligibleStrategy(units);
            String dozensName = CONSTANTS_MAPPING.get(dozens - units);
            String dozenHyphen = units > 0 ? "-" : "";
            return Stream.of(dozensName, dozenHyphen, unitsStrategy.processUnits(units))
                    .filter(s -> !s.isEmpty())
                    .collect(Collectors.joining(""));
        }
    };

    private Predicate<Integer> condition;

    EnglishDozensStrategy(Predicate<Integer> condition) {
        this.condition = condition;
    }

    public static EnglishDozensStrategy getEligibleStrategy(int number) {
        return Stream.of(values())
                .filter(isEligibleForNumber(number))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    private static Predicate<EnglishDozensStrategy> isEligibleForNumber(int number) {
        return strategy -> strategy.condition.test(number);
    }

    /**
     * Below listed an immutable map for map all exceptions, and base for creation dozen-unit pairs.
     */
    Map<Integer, String> CONSTANTS_MAPPING = ImmutableMap.<Integer, String>builder()
            .put(10, "ten").put(11, "eleven").put(12, "twelve").put(13, "thirteen").put(14, "fourteen")
            .put(15, "fifteen").put(16, "sixteen").put(17, "seventeen").put(18, "eighteen").put(19, "nineteen")
            .put(20, "twenty").put(30, "thirty").put(40, "forty").put(50, "fifty")
            .put(60, "sixty").put(70, "seventy").put(80, "eighty").put(90, "ninety")
            .build();
}
