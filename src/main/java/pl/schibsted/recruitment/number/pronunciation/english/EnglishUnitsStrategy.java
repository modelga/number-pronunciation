package pl.schibsted.recruitment.number.pronunciation.english;

import pl.schibsted.recruitment.number.pronunciation.UnitsStrategy;

import java.util.Optional;
import java.util.stream.Stream;

enum EnglishUnitsStrategy implements UnitsStrategy {
    ZERO(0, Optional.of("")), ONE(1), TWO(2), THREE(3), FOUR(4),
    FIVE(5), SIX(6), SEVEN(7), EIGHT(8), NINE(9);

    private Integer theNumber;
    private String pronunciation;

    EnglishUnitsStrategy(Integer theNumber) {
        this(theNumber, Optional.empty());
    }

    EnglishUnitsStrategy(Integer theNumber, Optional<String> pronunciation) {
        this.theNumber = theNumber;
        this.pronunciation = pronunciation.orElse(toString().toLowerCase());
    }

    public static EnglishUnitsStrategy getEligibleStrategy(int number) {
        return Stream.of(values())
                .filter(strategy -> strategy.theNumber == number)
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public String processUnits(int units) {
        return pronunciation;
    }
}
