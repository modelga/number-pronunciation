package pl.schibsted.recruitment.number.pronunciation;

public interface NumberPronouncer {
    String pronounce(Integer number);

}
