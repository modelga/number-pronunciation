package pl.schibsted.recruitment.number.pronunciation.english;

import org.springframework.stereotype.Component;
import pl.schibsted.recruitment.number.pronunciation.NumberPronouncer;
import pl.schibsted.recruitment.number.pronunciation.Pronouncer;
import pl.schibsted.recruitment.number.pronunciation.UnitsStrategy;

import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

@Component
@Pronouncer(name = "English Number Pronouncer", key = "en")
public class EnglishNumberPronouncer implements NumberPronouncer {

    private static final int GRADE_MULTIPLIER = 1000;
    private static final String HUNDRED = "hundred";
    private static final String SEPARATOR = " ";
    private static final String NEGATIVE_PREFIX = "minus";
    private static final String ZERO_NUMBER = "zero";

    @Override
    public String pronounce(Integer number) {

        if (number == 0)
            return ZERO_NUMBER;

        Stream<String> result = doPronounceInference(Math.abs(number));

        if (number < 0)
            result = Stream.concat(Stream.of(NEGATIVE_PREFIX), result);

        return result
                .filter(s -> !s.isEmpty())
                .collect(joining(SEPARATOR));
    }

    private Stream<String> doPronounceInference(Integer number) {
        Stream<String> result = Stream.of();

        int analyzingGrade = 1;
        int unProcessedValue = number;
        while (unProcessedValue > 0) {
            Stream<String> suffix = getGradeSuffix(analyzingGrade);

            int processingPart = unProcessedValue % GRADE_MULTIPLIER;
            if (processingPart > 0) {
                Stream<String> concatenated = Stream.concat(processPart(processingPart), suffix);
                result = Stream.concat(concatenated, result);
            }

            analyzingGrade = analyzingGrade * GRADE_MULTIPLIER;
            unProcessedValue = number / analyzingGrade;
        }
        return result;
    }

    private Stream<String> getGradeSuffix(int analyzingGrade) {
        return Stream.of(EnglishGradeSuffixStrategy.getEligibleStrategy(analyzingGrade).getGradeSuffix());
    }

    /**
     * As the input of this method is possible only the number between 1 and 999 (GRADE_MULTIPLIER - 1)
     *
     * @param part
     * @return
     */
    private Stream<String> processPart(Integer part) {
        return Stream.concat(processOverHundred(part / 100), processUnderHundred(part % 100));
    }

    private Stream<String> processOverHundred(int numberOfHundreds) {
        if (numberOfHundreds == 0)
            return Stream.of();
        UnitsStrategy strategy = EnglishUnitsStrategy.getEligibleStrategy(numberOfHundreds);
        return Stream.of(strategy.processUnits(numberOfHundreds), HUNDRED);

    }

    private Stream<String> processUnderHundred(int dozen) {
        EnglishDozensStrategy strategy = EnglishDozensStrategy.getEligibleStrategy(dozen);
        return Stream.of(strategy.processDozens(dozen));
    }
}

