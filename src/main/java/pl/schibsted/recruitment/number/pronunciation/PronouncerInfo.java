package pl.schibsted.recruitment.number.pronunciation;

import java.util.Optional;

public class PronouncerInfo {
    private final String key;
    private final String name;

    private PronouncerInfo(String key, String name) {
        this.key = key;
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public static PronouncerInfo createFromPronouncer(NumberPronouncer pronouncer) {
        Optional<Pronouncer> annotation = Optional.ofNullable(pronouncer.getClass().getAnnotation(Pronouncer.class));
        if (annotation.isPresent()) {
            return new PronouncerInfo(annotation.get().key(), annotation.get().name());
        } else {
            return new PronouncerInfo(String.valueOf(pronouncer.hashCode()), pronouncer.getClass().getSimpleName());
        }
    }
}
