package pl.schibsted.recruitment.number.pronunciation;

public interface GradeSuffixStrategy {
    String getGradeSuffix();
}
