package pl.schibsted.recruitment.number.pronunciation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.AbstractMap.SimpleEntry;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PronouncerService {
    private Collection<NumberPronouncer> pronouncers;

    @Value("${pronouncer.default}")
    private String defaultPronouncer;

    @Autowired
    public PronouncerService(Collection<NumberPronouncer> pronouncers) {
        this.pronouncers = pronouncers;
    }

    public Map<PronouncerInfo, NumberPronouncer> getPronouncersMap() {
        return pronouncers.stream()
                .map(t -> new SimpleEntry<>(PronouncerInfo.createFromPronouncer(t), t))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    public String doPronounce(Integer number, String key) {
        String npeSafeKey = Optional.ofNullable(key).orElse(defaultPronouncer);
        return getPronouncer(npeSafeKey).pronounce(number);
    }

    private NumberPronouncer getPronouncer(String key) {
        return getPronouncersMap().entrySet().stream()
                .filter(entry -> key.equals(entry.getKey().getKey()))
                .map(Map.Entry::getValue)
                .findAny()
                .orElseGet(() -> getPronouncer(defaultPronouncer));
    }


}
