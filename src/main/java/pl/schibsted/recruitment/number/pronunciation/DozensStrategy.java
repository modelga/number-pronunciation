package pl.schibsted.recruitment.number.pronunciation;

public interface DozensStrategy {
    String processDozens(int dozens);
}
