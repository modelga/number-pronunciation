package pl.schibsted.recruitment.number.pronunciation.english;

import pl.schibsted.recruitment.number.pronunciation.GradeSuffixStrategy;

import java.util.function.Predicate;
import java.util.stream.Stream;

enum EnglishGradeSuffixStrategy implements GradeSuffixStrategy {
    NON_GRADE(n -> n < 1000, ""),
    THOUSANDS(n -> n >= 1000 && n < 1000 * 1000, "thousand"),
    MILLIONS(n -> n >= 1000 * 1000 && n < 1000 * 1000 * 1000, "million"),
    BILLIONS(n -> n >= 1000 * 1000 * 1000, "billion");

    private final Predicate<Integer> condition;
    private final String suffix;

    EnglishGradeSuffixStrategy(Predicate<Integer> condition, String suffix) {
        this.condition = condition;
        this.suffix = suffix;
    }


    @Override
    public String getGradeSuffix() {
        return suffix;
    }

    private static Predicate<EnglishGradeSuffixStrategy> isEligibleForNumber(int number) {
        return gradeSuffix -> gradeSuffix.condition.test(number);
    }

    public static EnglishGradeSuffixStrategy getEligibleStrategy(int grade) {
        return Stream.of(values())
                .filter(isEligibleForNumber(grade))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
