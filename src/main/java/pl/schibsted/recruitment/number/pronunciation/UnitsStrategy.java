package pl.schibsted.recruitment.number.pronunciation;

public interface UnitsStrategy {
    String processUnits(int units);
}
