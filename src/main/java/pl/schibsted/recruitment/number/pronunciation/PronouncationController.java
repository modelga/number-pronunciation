package pl.schibsted.recruitment.number.pronunciation;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;
import java.util.Set;

@Controller
public class PronouncationController {

    private PronouncerService pronouncerService;

    @Autowired
    public PronouncationController(PronouncerService pronouncerService) {
        this.pronouncerService = pronouncerService;
    }


    @RequestMapping("list")
    @ResponseBody
    public Set<PronouncerInfo> getList() {
        return pronouncerService.getPronouncersMap().keySet();
    }

    @RequestMapping("pronounce/{number}")
    @ResponseBody
    public Map<String, String> doPronounce(@PathVariable("number") Integer number,
                                           @RequestParam(value = "key", required = false) String key) {
        return ImmutableMap.of("result", pronouncerService.doPronounce(number, key));
    }
}
