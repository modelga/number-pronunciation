$(document).ready(function () {

    $.get(
        "/list",
        function (data) {
            createSelect(data);
        }
    );

    $("#numberToPronounce").change(function (e) {
        var target = $(e.target);
        var INT_RANGE = Math.pow(2, 31);
        var parsedValue = Math.max(-INT_RANGE, Math.min(INT_RANGE - 1, parseInt(target.val())));
        target.val(isNaN(parsedValue) ? 0 : parsedValue);
        refreshPronounce();
    });

});

var refreshPronounce = function () {
    doPronounce($("#selectKey").val(), $("#numberToPronounce").val());
}

var createSelect = function (data) {
    var lookupTag = $("#selectKey");
    var displayableName = $("#selectName");
    lookupTag
        .find("option")
        .remove().end();
    $(data).each(function (o, i) {
        lookupTag.append('<option value="' + i["key"] + '">' + i["name"] + '</option>')
    });
    if (lookupTag.find("option").length == 1) {
        lookupTag.hide();
        displayableName.html(lookupTag.find("option").text())
        displayableName.show()
    } else {
        lookupTag.show()
        displayableName.hide()
    }
    refreshPronounce();
    $(lookupTag).change(function () {
        refreshPronounce()
    });
};


var doPronounce = function (key, number) {
    var result = $("#result");
    $.get("/pronounce/" + number + "?key=" + key,
        function (data) {
            result.text(data["result"]);
        });
};
